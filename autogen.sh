#!/bin/sh

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

ORIGDIR=`pwd`
cd $srcdir

DIE=0

# Check for autoconf, the required version is set in configure.in
(autoconf --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have at minimum autoconf version 2.12 installed. " 
	echo "Download the appropriate package for your distribution, "
	echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
}

# Check for libtool
(libtool --version | egrep "1.3") > /dev/null || 
(libtool --version | egrep "1.4") > /dev/null || {
	echo
	echo "You must have at minimum libtool version 1.3 installed"
	echo "Download the appropriate package for your distribution, "
	echo "or get the source tarball at "
	echo "ftp://alpha.gnu.org/gnu/libtool-1.4.tar.gz"
	DIE=1
}

# Check for automake, the required version is set in Makefile.am
(automake --version) < /dev/null > /dev/null 2>&1 ||{
	echo
	echo "You must have at minimum automake version 1.4 installed"
	echo "Download the appropriate package for your distribution, "
	echo "or get the source tarball at "
	echo "ftp://ftp.cygnus.com/pub/home/tromey/automake-1.4.tar.gz"
	DIE=1
}

if test "$DIE" -eq 1; then
	exit 1
fi

(test -f src/orbitals.idl) || {
	echo "You must run this script in the top-level directory"
	exit 1
}

libtoolize --copy --force
aclocal $ACLOCAL_FLAGS
automake --add-missing $am_opt
autoconf

cd $ORIGDIR

echo "Running $srcdir/configure --enable-maintainer-mode" "$@"
$srcdir/configure --enable-maintainer-mode "$@"

echo 
echo "Now type 'make' to compile orbitals."
