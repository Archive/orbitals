/*
 * orbitals - ORBit benchmarking.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Mark McLoughlin <mark@skynet.ie>
 */

#include <stdio.h>
#include <bench.h>

#include "orbitals-config.h"
#include "orbitals.h"

#define IOR_SZ 1024

static void
bench_orbitals (CORBA_Object       obj,
		glong              iters,
		CORBA_Environment *ev)
{
	orbitals_OctetSeq *seq;

	BENCH (orbitals_bench1 (obj, ev), LONGER);
	micro ("simple", get_n ());
	BENCH (orbitals_bench2 (obj, ev), LONGER);
	micro ("simple oneway", get_n ());

	seq           = orbitals_OctetSeq__alloc();
	seq->_length  = seq->_maximum = 10;
	seq->_buffer  = CORBA_sequence_CORBA_octet_allocbuf (seq->_length);
	seq->_release = CORBA_TRUE;
	BENCH (orbitals_bench3 (obj, seq, ev), LONGER);
	micro ("simple sequence", get_n ());
	CORBA_free (seq);

	seq           = orbitals_OctetSeq__alloc();
	seq->_length  = seq->_maximum = 4096;
	seq->_buffer  = CORBA_sequence_CORBA_octet_allocbuf (seq->_length);
	seq->_release = CORBA_TRUE;
	BENCH (orbitals_bench3 (obj, seq, ev), LONGER);
	micro ("long sequence", get_n ());
	CORBA_free (seq);
}

static void 
read_ior (gchar *ior)
{
	FILE  *iorfile;
	gint   size;

	if (!(iorfile = fopen (".ior", "rb")))
		g_error ("Cannot open .ior - run the server\n");

	size = fread (ior, 1, IOR_SZ, iorfile);
	ior[size] = '\0';

	fclose (iorfile);
}
	
int main (int argc, char **argv)
{
	CORBA_Environment  ev;
	CORBA_ORB          orb;
	CORBA_Object       orbitals_obj;
	gchar              ior[IOR_SZ+1];

	CORBA_exception_init (&ev);

	orb = CORBA_ORB_init (&argc, argv, "", &ev);

	read_ior (ior);

	orbitals_obj = CORBA_ORB_string_to_object (orb, ior, &ev);

	bench_orbitals (orbitals_obj, 1000, &ev);

	orbitals_shutdown (orbitals_obj, &ev);

	CORBA_Object_release (orbitals_obj, &ev);

	CORBA_Object_release ((CORBA_Object)orb, &ev);

	return 0;
}
