/*
 * orbitals - ORBit benchmarking.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Mark McLoughlin <mark@skynet.ie>
 */

#include <stdio.h>
#include <stdlib.h>

#include "orbitals-config.h"
#include "orbitals.h"

static void orbitals_bench1_impl   (PortableServer_Servant      servant,
				    CORBA_Environment          *ev) {}
static void orbitals_bench2_impl   (PortableServer_Servant      servant,
				    CORBA_Environment          *ev) {}
static void orbitals_bench3_impl   (PortableServer_Servant      servant,
				    const orbitals_OctetSeq    *seq,
				    CORBA_Environment          *ev) {}
static void orbitals_shutdown_impl (PortableServer_Servant      servant,
				    CORBA_Environment          *ev);

static CORBA_ORB orb;

static PortableServer_ServantBase__epv base_epv = {
	._private    = NULL,
	.finalize    = NULL,
	.default_POA = NULL
};

static POA_orbitals__epv orbitals_epv = {
	._private = NULL,
	.bench1   = orbitals_bench1_impl,
	.bench2   = orbitals_bench2_impl,
	.bench3   = orbitals_bench3_impl,
	.shutdown = orbitals_shutdown_impl
};

static POA_orbitals__vepv orbitals_vepv = {
	._base_epv     = &base_epv,
	.orbitals_epv  = &orbitals_epv
};

static POA_orbitals orbitals_servant = {
	._private = NULL,
	.vepv     = &orbitals_vepv
};

static void
write_ior (gchar *ior)
{
	FILE *iorfile;

	if (!(iorfile = fopen (".ior", "wb")))
		g_error ("Cannot open .ior\n");

	fwrite (ior, strlen (ior), 1, iorfile);

	fclose (iorfile);
}

int main (int argc, char **argv)
{
	CORBA_Environment          ev;
	PortableServer_POA         poa;
	PortableServer_POAManager  poa_mgr;
	PortableServer_ObjectId   *orbitals_objid; 
	orbitals                   orbitals_obj;
	char                      *ior;

	CORBA_exception_init (&ev);

	orb = CORBA_ORB_init (&argc, argv, "", &ev);
	if (!orb)
		return 1;

	POA_orbitals__init (&orbitals_servant, &ev);

	poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (orb, "RootPOA", &ev);
	if (!poa)
		return 1;
 
	poa_mgr = PortableServer_POA__get_the_POAManager (poa, &ev);
	if (!poa_mgr)
		return 1;

	orbitals_objid = PortableServer_POA_activate_object (poa, 
							     &orbitals_servant,
							     &ev);
	CORBA_free (orbitals_objid);
 
	orbitals_obj = PortableServer_POA_servant_to_reference (poa, 
								&orbitals_servant,
								&ev);
	if (!orbitals_obj)
		return 1;

	ior = CORBA_ORB_object_to_string (orb, orbitals_obj, &ev);
	if (!ior)
		return 1;

	write_ior (ior);

	CORBA_free (ior);

	PortableServer_POAManager_activate (poa_mgr, &ev);

	CORBA_ORB_run (orb, &ev);

	CORBA_Object_release (orbitals_obj, &ev);
	CORBA_Object_release ((CORBA_Object)poa_mgr, &ev);
	CORBA_Object_release ((CORBA_Object)poa, &ev);
	CORBA_Object_release ((CORBA_Object)orb, &ev);

	return 0;
}

static void
orbitals_shutdown_impl (PortableServer_Servant      servant,
			CORBA_Environment          *ev)
{
	CORBA_ORB_shutdown (orb, CORBA_FALSE, ev);
}
